
import RMI.RMI;
import java.awt.Point;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.Icon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bruno R
 */
public class Server extends UnicastRemoteObject implements RMI {
    boolean player1 = false;    
    
    public Server() throws RemoteException{
        super();
    }
    
    @Override
    public void pieceTakedAdvertise(String player) throws RemoteException {
        
    }
    
    @Override
    public void trilhaAdvertise(String player) throws RemoteException {
       
    }
    
    @Override
    public void winnerCtrl(String player) throws RemoteException {
       
    }
    
    @Override
    public void restartRequest(String player, String answer) throws RemoteException {
       
    }

    @Override
    public void restartResponse(String player, String answer) throws RemoteException {
      
    }
    
    @Override
    public void giveUpCtrl(String player, String reason) throws RemoteException {
       
    }
    
    @Override
    public void protocolTurnCtrl(String player) throws RemoteException {
        
    }
    
    @Override
    public void protocolGameCtrl(String player, Point nextLocation, Point lastLocation, Icon nextIconState, Icon lastIconState) throws RemoteException {
        
    }
    
    @Override
    public String startComunication() throws RemoteException {
        if (player1 == false){
            
            player1 = true;
            return "Player1#server1#1098#server2#1099";            
        }
        
        return "Player2#server2#1099#server1#1098";          

    }
    
    @Override
    public void protocolMsgCtrl(String player, String message) throws RemoteException {
        
        
    }
    
    @Override
    public String protocolResponse() throws RemoteException {
        return "";
    }
    
    public static void main(String[] args) {
        try{
            
            Registry reg = LocateRegistry.createRegistry(1250);
            reg.rebind("server", new Server());
            
            System.out.println("Servidor Gerenciador Iniciado");
            
        }catch(Exception e){
            System.out.println(e);
        }
    }       
}
