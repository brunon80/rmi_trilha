package RMI;


import java.awt.Point;
import java.rmi.Remote;
import java.rmi.RemoteException;
import javax.swing.Icon;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bruno R
 */
public interface RMI extends Remote{
    
    public String startComunication() throws RemoteException;
    public void protocolMsgCtrl(String player, String message) throws RemoteException;
    public void protocolGameCtrl(String player, Point nextLocation, Point lastLocation, Icon nextIconState, Icon lastIconState)throws RemoteException;
    public void protocolTurnCtrl(String player) throws RemoteException;
    public void giveUpCtrl(String player, String reason) throws RemoteException;
    public void restartRequest(String player, String answer) throws RemoteException;
    public void restartResponse(String player, String answer) throws RemoteException;
    public void winnerCtrl(String player) throws RemoteException;
    public void trilhaAdvertise(String player) throws RemoteException;
    public void pieceTakedAdvertise(String player)throws RemoteException;
    public String protocolResponse() throws RemoteException;
    
}
