/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;


import RMI.RMI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.Icon;
import javax.swing.JButton;
import static RMI.Client.*;

/**
 * Classe destinda a verificar eventos que ocorrem na tela
 * @author Bruno R
 */
   public class ListenForButton implements ActionListener, WindowListener, KeyListener{

    /**
     * 
     * @param e: origem do clique
     */
    @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(myPieces);
            
            
            
            if (myPieces == 2 && advertise == false){
                advertise = true;
                chatArea.append("<Moderador> A próxima é sua última peça, use com sabedoria!\n");
                chatArea.append("\n");
            }
            
            
            if (myPieces == 0 && nextPart == false) {
                              
                chatArea.append("<Moderador> Você não tem mais peças!, hora de movimenta-las.\n");
                chatArea.append("\n");
                nextPart = true;
                System.out.println("segunda rodada");
            }
            if (turn){
                tempbtn = (JButton) e.getSource();
                
                
                if (myPieces >0 && tempbtn.getIcon().toString().equals(noIcon.toString()) && nextPart == false){
                    tempbtn.setIcon(myColorPieces);
                    System.out.println("primeiro");
                    takebMinIcon();
                    trilha(tempbtn);
                }

                if(tempbtn != lastButton && !tempbtn.getIcon().toString().equals(yourColorPieces.toString()) && nextPart == false){
                    //System.out.println("entrei");
                    System.out.println("terceiro"); 
                    tempbtn.setIcon(myColorPieces);
                    lastButton.setIcon(noIcon);
                         
                }
                
                
                
                if (tempbtn.getIcon().toString().equals(yourColorPieces.toString()) && nextPart == false){
                    System.out.println("quarto"); 
                    if (!tempbtn.getIcon().equals(myColorPieces)){
                        tempbtn.setIcon(noIcon);
                        //ComunicationCtrl.pieceTakedAdvertise();
                        try{
//                     
                            Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                            RMI rmi = (RMI) reg.lookup(yourServer);
                            rmi.pieceTakedAdvertise(player);

                         }catch(Exception ex){


                         }
                        putwMinIcon();
                        myPiecesTaked ++;
                        winner();
                        
                    }
                    
                    
                }
                

                if (nextPart == true){                        
                    
                    
                    
                    if (tempbtn.getIcon() != redIcon && lastButton.getIcon() == myColorPieces){
                        System.out.println("primeiro");
                        if (tempbtn.getIcon().toString().equals(noIcon.toString()) && lastButton.getIcon() == myColorPieces){
                            lastButton.setIcon(noIcon);
                            tempbtn.setIcon(myColorPieces);
                            trilha(tempbtn);
                            System.out.println("Segundo");
                        }

                        if (!tempbtn.getIcon().toString().equals(noIcon.toString()) && lastButton.getIcon() == myColorPieces && !tempbtn.getIcon().toString().equals(myColorPieces.toString())){
                            
                            if (tempbtn.getIcon() != lastButton.getIcon()){
                                tempbtn.setIcon(noIcon);
                                //ComunicationCtrl.pieceTakedAdvertise();
                                try{
//                     
                                    Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                                    RMI rmi = (RMI) reg.lookup(yourServer);
                                    rmi.pieceTakedAdvertise(player);
                                    
                                 }catch(Exception ex){


                                 }
                                putwMinIcon();
                                myPiecesTaked ++;
                                winner();
                            }
                        }
                    }
                }
//                connect.protocolGameCtrl(tempbtn.getLocation(), lastButton.getLocation(), tempbtn.getIcon(), lastButton.getIcon());
                
                try{
//                     connect.protocolMsgCtrl(msgToSend);
                   Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                    RMI rmi = (RMI) reg.lookup(yourServer);
                   rmi.protocolGameCtrl(player, tempbtn.getLocation(), lastButton.getLocation(), tempbtn.getIcon(), lastButton.getIcon());

                }catch(Exception ex){


                }

                if (tempbtn.getIcon() != noIcon){
                
                    lastButton = tempbtn;
                }
                
                
            }
            else{
                chatArea.append("<Moderador> Não é sua vez. Aguarde! \n");
                chatArea.append("\n");
            }
    
        
    }

    /**
     *
     * @param e: origem do clique
     */
    @Override
        public void windowOpened(WindowEvent e) {
        }

    /**
     * envia a menssagem que fechou a janela antes dela fechar
     * @param e: origem do clique
     */
    @Override
        public void windowClosing(WindowEvent e) 
        {
        //ComunicationCtrl cm = new ComunicationCtrl();

            //cm.giveUpCtrl("O outro jogador fechou a janela! =D");
            try{
//                     connect.protocolMsgCtrl(msgToSend);
               Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
               RMI rmi = (RMI) reg.lookup(yourServer);
               rmi.giveUpCtrl(player, "O outro jogador fechou a janela! =D");

            }catch(Exception ex){

                System.out.println("será q fechou?");
            }
  
        }

    /**
     *
     * @param e
     */
    @Override
        public void windowClosed(WindowEvent e) {
            
        }

    /**
     *
     * @param e
     */
    @Override
        public void windowIconified(WindowEvent e) {
            
        }

    /**
     *
     * @param e
     */
    @Override
        public void windowDeiconified(WindowEvent e) {
        }

    /**
     *
     * @param e
     */
    @Override
        public void windowActivated(WindowEvent e) {
            
        }

    /**
     *
     * @param e
     */
    @Override
        public void windowDeactivated(WindowEvent e) {
            
        }
        
    /**
     *
     * @param e
     */
    @Override
        public void keyTyped(KeyEvent e) {
            
        }

    /**
     * Envia menssagem pressionando "Enter" do teclado
     * @param e: : origem do clique
     */
    @Override
        public void keyPressed(KeyEvent e) {
            //System.out.println(e.getKeyCode());
            if(e.getKeyCode() == 10){
            
            msgToSend = msgField.getText();
            try{
//                     connect.protocolMsgCtrl(msgToSend);
                   Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                   RMI rmi = (RMI) reg.lookup(yourServer);
                   rmi.protocolMsgCtrl(player,msgToSend);
                   chatArea.append("<Eu> " + msgToSend +"\n");
                   chatArea.append("\n");
                   msgField.setText("");

                }catch(Exception ex){


                }
            }
        }

    /**
     *
     * @param e
     */
    @Override
        public void keyReleased(KeyEvent e) {
            
        }
                
}
