/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;

import RMI.RMI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.TimeUnit;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import static RMI.Client.*;

/**
 *
 * @author Bruno R
 */
   public class ListenForToolBarButton implements ActionListener{
   
    /**
     * Classe que "escuta" eventos na Tollbar
     * @param e: origem do clique
     */
    @Override
        public void actionPerformed(ActionEvent e) {
        
            if (turn) {
               if(e.getSource() == newTurn){
//                   connect.protocolMsgCtrl("passou a vez");
//                    connect.protocolTurnCtrl();
                    try{
//                     connect.protocolMsgCtrl(msgToSend);
                        Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                        RMI rmi = (RMI) reg.lookup(yourServer);
                        rmi.protocolMsgCtrl(player, "Passou a vez");
                        TimeUnit.MILLISECONDS.sleep(100);
                        rmi.protocolTurnCtrl(player);                        

                     }catch(Exception ex){


                     }
                    turn = false;
                    if (nextPart == false) {
                        
                            lastButton = new JButton();                            
                        
                       
                        System.out.println("passei na primeira parte");

                            myPieces --;

                    }
                    if (nextPart == true) {

                        System.out.println("passei na segunda parte");
//                        connect.protocolGameCtrl(tempbtn.getLocation(), lastButton.getLocation(), tempbtn.getIcon(), lastButton.getIcon());
                    }
                
                }
               if (e.getSource() == back){
                   rollBack();
               }
               
               
               if (e.getSource() == giveUp){
               
                   GiveUpWindow giveUp = new GiveUpWindow("Tem certeza que quer desistir?", "giveup");

               }
               
               if (e.getSource() == restart){
               
                   GiveUpWindow giveUp = new GiveUpWindow("Tem certeza que deseja Reiniciar?", "restart");
                   
               }
           }else{
                chatArea.append("<Moderador> Não é sua vez. Aguarde! \n");
                chatArea.append("\n");
               }
            if (e.getSource() == tutorial){
               
                   new HelpWindow();
               }
            
            if (e.getSource() == send){
                
                msgToSend = msgField.getText();
                try{
//                       connect.protocolMsgCtrl(msgToSend);
                        Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                        RMI rmi = (RMI) reg.lookup(yourServer);
                        rmi.protocolMsgCtrl(player,msgToSend);
                       chatArea.append("<Eu> " + msgToSend +"\n");
                       chatArea.append("\n");
                       msgField.setText("");

                    }catch(Exception ex){


                    }
            }
            
        }
        
        
       
   }
