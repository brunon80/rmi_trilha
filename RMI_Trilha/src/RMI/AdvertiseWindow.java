/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;
import javax.swing.JOptionPane;
/**
 * Classe destinada a mostrar alertas sobre vitorias ao jogador
 * @author Bruno R
 */
public class AdvertiseWindow {
    
    String reason;
    
    /**
     *
     * @param reason: esse parametro represna por qal motivo o jogador venceu
     */
    public AdvertiseWindow(String reason){
        
        this.reason = reason;
        JOptionPane.showMessageDialog(null, "Parabéns, você venceu!, " + reason);
        
    }
}
