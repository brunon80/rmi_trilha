/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;

import static RMI.Client.btnArray;
import static RMI.Client.chatArea;
import static RMI.Client.dataProtocolRecivied;
import static RMI.Client.lastIcon;
import static RMI.Client.lastRecivied;
import static RMI.Client.msg;
import static RMI.Client.nextIcon;
import static RMI.Client.nextRecivied;
import static RMI.Client.noIcon;
import static RMI.Client.player;
import static RMI.Client.port;
import static RMI.Client.portToTalk;
import static RMI.Client.restart;
import static RMI.Client.server;
import static RMI.Client.turn;
import static RMI.Client.yourServer;
import java.awt.Image;
import java.awt.Point;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Bruno R
 */
public class MyRMIServer extends UnicastRemoteObject implements RMI{

    public MyRMIServer() throws RemoteException{
        super();
    }
    
    @Override
    public void pieceTakedAdvertise(String player) throws RemoteException {
        chatArea.append("<Moderador> " + player + " Tirou uma peça sua! \n");
        chatArea.append("\n");
    }
    
    @Override
    public void trilhaAdvertise(String player) throws RemoteException {
        chatArea.append("<Moderador> " + player + " Fez trilha!!! Você vai perder uma peça :/ \n");
        chatArea.append("\n");
    }
    
    @Override
    public void winnerCtrl(String player) throws RemoteException {
        JOptionPane.showMessageDialog(null, "Que pena, você perdeu! :/");
        restart();
    }
    
    @Override
    public void restartRequest(String player, String answer) throws RemoteException {
        if (answer.equals("yes")){

            GiveUpWindowForSure giveUpfSure = new GiveUpWindowForSure("O outro jogador deseja reiniciar a partida, você concorda?");
            if (answer.equals(giveUpfSure.myAnswer)){
                //ComunicationCtrl cm = new ComunicationCtrl();
                //cm.restartResponse("yes");                                         
//                                                      
                   
                try{
        //          connect.protocolMsgCtrl(msgToSend);
                    Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                    RMI rmi = (RMI) reg.lookup(yourServer);
                    rmi.restartResponse(player, "yes");

                 }catch(Exception ex){


                }

                restart();
            }
       }
    }

    @Override
    public void restartResponse(String player, String answer) throws RemoteException {
       if (answer.equals("yes")){

            restart();
            chatArea.append("<Moderador> O outro jogador concordou em reiniciar a partida!\n");
            chatArea.append("\n");

        }
    }
    
    @Override
    public void giveUpCtrl(String player, String reason) throws RemoteException {
        AdvertiseWindow adv = new AdvertiseWindow(reason);
        restart();
    }
    
    @Override
    public void protocolTurnCtrl(String player) throws RemoteException {
        turn = true;
    }
    
    @Override
    public void protocolGameCtrl(String player, Point nextLocation, Point lastLocation, Icon nextIconState, Icon lastIconState) throws RemoteException {
                           System.out.println("entrei em game");
                           nextRecivied = nextLocation.toString();
                           System.out.println("passei 1");
                           lastRecivied = lastLocation.toString();
                           System.out.println("passei 2");
                           nextIcon = (ImageIcon) nextIconState;
                           System.out.println("passei 3");
                           //System.out.println(dataProtocolRecivied[5].toString());
                           lastIcon = (ImageIcon) lastIconState;
                           System.out.println("passei 4");

                           //System.out.println(msg);
                           //System.out.println(A0.getLocation().toString());
                             for (int i = 0; i< btnArray.length; i++){
                                 if (btnArray[i].getLocation().toString().equals(nextRecivied)){ 

                                     btnArray[i].setIcon(nextIcon);                            
                                 }

                             }

                             for (int i = 0; i< btnArray.length; i++){

                                 if(btnArray[i].getLocation().toString().equals(lastRecivied)){

                                     if (lastIcon == null) lastIcon = noIcon;
                                     btnArray[i].setIcon(lastIcon);
                                }
                            }
    }
    
    @Override
    public String startComunication() throws RemoteException {
        
        return "";          

    }
    
    @Override
    public void protocolMsgCtrl(String player, String message) throws RemoteException {
        
        msg = message;
        chatArea.append("<" + player + ">" + " "  + msg +"\n");
        chatArea.append("\n");
    }
    
    @Override
    public String protocolResponse() throws RemoteException {
       
        return "";
    }
    
    public void startMyServer(){
    
        try{
            
            Registry reg = LocateRegistry.createRegistry(Integer.parseInt(port));
            reg.rebind(server, new MyRMIServer());
            
            System.out.println("Servidor Iniciado");
            
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
