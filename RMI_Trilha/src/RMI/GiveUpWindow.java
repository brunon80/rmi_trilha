/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RMI;

import RMI.RMI;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.JOptionPane;
import static RMI.Client.*;
/**
 * Janela de desistencia
 * @author Bruno R
 */
public class GiveUpWindow{
    //ComunicationCtrl cm = new ComunicationCtrl();
    String message;
    String origin;
    
    /**
     * metodo que invoca a tela de desistencia
     * @param message: menssagem a ser recebida e mostrada
     * @param origin: de onde vem a menssagem
     */
    public GiveUpWindow(String message, String origin){
        
        this.message = message;
        this.origin = origin;
        //JDialog.setDefaultLookAndFeelDecorated(true);
        int response = JOptionPane.showConfirmDialog(null, message, "Tem certeza?",
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.NO_OPTION) {
          if (origin.equals("restart")){
                
                //cm.restartRequest("no");
                try{
    //                     connect.protocolMsgCtrl(msgToSend);
                   Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                   RMI rmi = (RMI) reg.lookup(yourServer);
                   rmi.restartRequest(player, "no");

                }catch(Exception ex){

                    
                }
                
            }
        } else if (response == JOptionPane.YES_OPTION) {
           
            if (origin.equals("restart")){
                
                //cm.restartRequest("yes");
                try{
    //             connect.protocolMsgCtrl(msgToSend);
                   Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                   RMI rmi = (RMI) reg.lookup(yourServer);
                   rmi.restartRequest(player, "yes");

                }catch(Exception ex){

                    
                }
   
            }
            if (origin.equals("giveup")){
                
                //cm.giveUpCtrl("O outro jogador desistiu!");
                try{
    //                     connect.protocolMsgCtrl(msgToSend);
                   Registry reg  = LocateRegistry.getRegistry("127.0.0.1", Integer.parseInt(portToTalk));
                   RMI rmi = (RMI) reg.lookup(yourServer);
                   rmi.giveUpCtrl(player, "O outro jogador desistiu!");

                }catch(Exception ex){

                   
                }
                    restart();
            }
          
        } else if (response == JOptionPane.CLOSED_OPTION) {
          System.out.println("JOptionPane closed");
        }
  

    }
}


